from flask import Flask
from flasgger import Swagger
from api.route.parking_bay import api
import os

def app():
    app = Flask(__name__)
    # env variable to override melbourne data api key
    env_melbourne_data_api_key = os.environ.get("MELBOURNE_DATA_API_KEY")
    if(env_melbourne_data_api_key):
        app.config['MELBOURNE_DATA_API_KEY'] = env_melbourne_data_api_key

    app.config['SWAGGER'] = {
        'title': 'Flask API Melbourne Parking Bays',
    }
    swagger = Swagger(app)

    app.register_blueprint(api, url_prefix='/api')

    return app
app = app()

if __name__ == '__main__':
    port = os.getenv('PORT',5000)

    app.run(host='0.0.0.0', port=port, debug=True)
