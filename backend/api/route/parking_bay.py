from http import HTTPStatus
from flask import Blueprint, jsonify, request
from flasgger import swag_from
from api.integration.melbourne_data import MelbourneData
from api.schema.parking_bay import ParkingBaySchema
from api.schema.location import Location


api = Blueprint("api", __name__)


@api.route("/bays")
@swag_from(
    {
        "responses": {
            HTTPStatus.OK.value: {"description": "Return list of parking bays within given radius", "schema": ParkingBaySchema}
        }
    }
)
def get_parking_bays():
    """
    input the location and distance(meter) to get parking bays with in the range
    """
    lat = request.args["lat"]
    lng = request.args["lng"]
    radius = request.args["radius"]
    limit = request.args.get("limit")
    limit = limit if limit else 5
    location = Location(lat, lng)
    result = MelbourneData().get_parking_bays(location, radius, limit)
    bays = [ParkingBaySchema().dump(bay) for bay in result]
    return jsonify(bays), 200
@api.route("/bays-within-box")
@swag_from(
    {
        "responses": {
            HTTPStatus.OK.value: {"description": "Return list of parking bays within given North West point and South East point", "schema": ParkingBaySchema}
        }
    }
)
def get_parking_bays_within_box():
    """
    input the location and distance(meter) to get parking bays with in the range
    """
    nw_lat = request.args["nw_lat"]
    nw_lng = request.args["nw_lng"]
    se_lat = request.args["se_lat"]
    se_lng = request.args["se_lng"]
    limit = request.args.get("limit")
    limit = limit if limit else 5
    nw_location = Location(nw_lat, nw_lng)
    se_location = Location(se_lat, se_lng)
    result = MelbourneData().get_parking_bays_withinBox(nw_location, se_location, limit)
    bays = [ParkingBaySchema().dump(bay) for bay in result]
    return jsonify(bays), 200


@api.errorhandler(Exception)
def handle_errors(e):
    try:
        print(e)
        return jsonify(error=str(e)), e.code
    except:
        print(e)
        return jsonify({"error":"Something went wrong"}), 500
