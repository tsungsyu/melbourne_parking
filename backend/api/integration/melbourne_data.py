import requests
from api.schema.location import Location
from flask import current_app


class MelbourneData:
    def __init__(self):
        api_key = current_app.config['MELBOURNE_DATA_API_KEY']
        if api_key:
            self.timeout = (3.05, 27)
            self.api_key = api_key
            self.base_url = "https://data.melbourne.vic.gov.au/resource"
        else:
            raise ValueError("Melbourne City Data API key is required")

    def get_parking_bays(self, location: Location, radius: int, limit: int):
        headers = {"X-App-Token": self.api_key}
        params = dict({"$limit": limit})
        params["$where"] = f"within_circle(location,{location.lat},{location.lng},{radius})"
        response = requests.get(
            f"{self.base_url}/vh2v-4nfs.json",
            params=params,
            headers=headers,
            timeout=self.timeout,
        )
        response.raise_for_status()
        return response.json()

    def get_parking_bays_withinBox(self, nw_location: Location, se_location: Location, limit: int):
        headers = {"X-App-Token": self.api_key}
        params = dict({"$limit": limit})
        params["$where"] = f"within_box(location,{nw_location.lat},{nw_location.lng},{se_location.lat},{se_location.lng})"
        response = requests.get(
            f"{self.base_url}/vh2v-4nfs.json",
            params=params,
            headers=headers,
            timeout=self.timeout,
        )
        response.raise_for_status()
        return response.json()

    def get_parking_bays_info(self, bay_ids: list):
        headers = {"X-App-Token": self.api_key}
        params = dict()
        separator = " OR bayid="
        params["$where"] = f"bayid={separator.join(bay_ids)}"
        response = requests.get(
            f"{self.base_url}/ntht-5rk7.json",
            params=params,
            headers=headers,
            timeout=self.timeout,
        )
        response.raise_for_status()
        return response
