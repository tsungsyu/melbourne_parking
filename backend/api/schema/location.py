from marshmallow.fields import Float,Str,Nested
from flask_marshmallow import Schema
from api.schema.human_address import HumanAddressSchema

class Location:
    def __init__(self, lat: float, lng: float):
        self.lng = lng
        self.lat = lat


class LocationSchema(Schema):
    class Meta:
        # Fields to exposeS
        fields = ["latitude","longitude"]

    latitude = Float()
    longitude = Float()
    human_address = Nested(HumanAddressSchema)
