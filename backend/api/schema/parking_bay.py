from flask_marshmallow import Schema
from marshmallow.fields import Nested, Str
from api.schema.location import LocationSchema


class ParkingBaySchema(Schema):
    bay_id = Str()
    st_marker_id = Str()
    status = Str()
    location = Nested(LocationSchema)