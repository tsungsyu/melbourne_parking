from marshmallow.fields import Float,Str
from flask_marshmallow import Schema

class HumanAddressSchema(Schema):
    address = Str()
    city = Str()
    state = Str()
    zip = Str()