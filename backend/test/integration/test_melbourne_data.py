from unittest import TestCase
from app import create_app
from api.integration.melbourne_data import MelbourneData
from api.schema.location import Location
from api.schema.parking_bay import ParkingBaySchema
import json


class TestWelcome(TestCase):
    def setUp(self):
        self.app = create_app().test_client()

    def test_get_parking_bays(self):
        location = Location(-37.807647, 144.966092)
        response = MelbourneData().get_parking_bays(location, 50, 2)
        for a in response:
            print(a)
        new_bays = [ParkingBaySchema().dump(bay_json) for bay_json in response]

        print(new_bays)
        for bay in new_bays:
            print(type(bay))
        self.assertTrue(response)

    def test_get_parking_bays_info(self):
        bay_ids = ["8779", "8780"]
        response = MelbourneData().get_parking_bays_info(bay_ids)
        self.assertTrue(response)
