import React, { useState, useEffect } from "react";
import axios from "axios";
import GoogleMapReact from "google-map-react";
import Marker from "./Marker";
import LoadingOverlay from "./LoadingOverlay";
const defaultCenter = { lat: -37.81626, lng: 144.955837 };
const defaultZoom = 18;
const availableStatus = "Unoccupied";
const OPTIONS = {
  minZoom: 16,
  maxZoom: 20,
};

const MelbourneMap = (props) => {
  const [loading, setLoading] = useState(false);
  const [center, setCenter] = useState({ lat: -37.81626, lng: 144.955837 });
  const [zoom, setZoom] = useState(19);
  const [markers, setMarkers] = useState([]);
  const [nwBound, setNwBound] = useState();
  const [seBound, setSeBound] = useState();
  useEffect(() => {
    if (nwBound && seBound) {
      setLoading(true);
      axios
        .get("/api/bays-within-box", { params: { nw_lng: nwBound.lng, nw_lat: nwBound.lat, se_lng: seBound.lng, se_lat: seBound.lat, limit: 500 } })
        .then((result) => {
          if (result.data) {
            setMarkers(
              result.data
                .filter((el) => el.status === availableStatus)
                .map((bay, index) => {
                  return <Marker zoom={zoom} key={index} lat={bay.location.latitude} lng={bay.location.longitude} />;
                })
            );
          }
        })
        .finally(() => setLoading(false));
    } else {
      setLoading(true);
      axios
        .get("/api/bays", { params: { lng: center.lng, lat: center.lat, radius: 300, limit: 50 } })
        .then((result) => {
          if (result.data) {
            setMarkers(
              result.data
                .filter((el) => el.status === availableStatus)
                .map((bay, index) => {
                  return <Marker zoom={zoom} key={index} lat={bay.location.latitude} lng={bay.location.longitude} />;
                })
            );
          }
        })
        .finally(() => setLoading(false));
    }
  }, [center.lat, center.lng, zoom, nwBound, seBound]);

  return (
    <LoadingOverlay active={loading}>
      <div data-testid="melbourne_map" style={{ height: "100vh", width: "100%" }}>
        <GoogleMapReact
          options={OPTIONS}
          onChange={({ center, zoom, bounds }) => {
            setNwBound({ lat: bounds.nw.lat, lng: bounds.nw.lng });
            setSeBound({ lat: bounds.se.lat, lng: bounds.se.lng });
            setCenter({ lat: center.lat, lng: center.lng });
            setZoom(zoom);
          }}
          bootstrapURLKeys={{ key: "AIzaSyCaGWXj_mcAsouQooWbH2A7NhJOZLjAUYM" }}
          defaultCenter={defaultCenter}
          defaultZoom={defaultZoom}
        >
          {markers}
        </GoogleMapReact>
      </div>
    </LoadingOverlay>
  );
};

export default MelbourneMap;
