import React from "react";
import MeterIcon from "../image/parking-meter.png";

const Marker = (props) => {
  const width = (props.zoom / 19) * 48;
  const iconStyle = {
    width: `${width}px`,
    height: `${width}px`,
  };

  return <img style={iconStyle} src={MeterIcon} alt="available" />;
};

export default Marker;
