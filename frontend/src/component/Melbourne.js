import React from "react";
import MelbourneMap from "./MelbourneMap";
import { Container, Row, Col } from "react-bootstrap";

export default function Melbourne() {
  return (
    <Container>
      {/* <Row>
        <Col className="header">Melbourne CBD</Col>
      </Row> */}
      <Row>
        <Col>
          <MelbourneMap />
        </Col>
      </Row>
    </Container>
  );
}
