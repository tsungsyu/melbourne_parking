import LoadingOverlay from 'react-loading-overlay'
import PacmanLoader from 'react-spinners/PacmanLoader'
 
export default function MyLoader({ active, children }) {
  return (
    <LoadingOverlay
      active={active}
      spinner={<PacmanLoader color={"#fff"} />}
    >
      {children}
    </LoadingOverlay>
  )
}