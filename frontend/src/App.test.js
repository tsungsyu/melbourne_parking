import { render, screen } from "@testing-library/react";
import App from "./App";

test("Melbourne map exists", () => {
  render(<App />);
  const melbourneMap = screen.getByTestId(/melbourne_map/i);

  expect(melbourneMap);
});
