import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Melbourne from "./component/Melbourne";

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/">
            <Melbourne />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
