
# Heroku dynos have at least 4 cores
worker_processes 4;

events {
    use epoll;
    accept_mutex on;
    worker_connections 1024;
}

http {
    gzip on;
    gzip_comp_level 2;
    gzip_min_length 512;
    server_tokens off;

    log_format main '$time_iso8601 - $status $request - client IP: $http_x_forwarded_for - to $upstream_addr - upstream status: $upstream_status, upstream_response_time $upstream_response_time, request_time $request_time';
    access_log /dev/stdout main;
    # set the following to "debug" when diagnosing an issue
    error_log /dev/stdout notice;
    log_not_found on;

    include mime.types;
    default_type application/octet-stream;
    sendfile on;

    # Must read the body in 5 seconds.
    client_body_timeout 5;

    # handle SNI
    proxy_ssl_server_name on;
    # resolver needs to be set because we're using dynamic proxy_pass
    resolver 8.8.8.8;

    upstream upstream {
        server ${SERVER_NAME}:443;
    }

    server {
        listen ${PORT};
        server_name _;

        # path for static files, React build files is going to be moved here
        root /usr/share/nginx/html;

        location /api {
            set $upstream upstream;
            proxy_pass https://$upstream/$request_uri;
            proxy_ssl_name ${SERVER_NAME};
            proxy_set_header x-forwarded-host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Host ${SERVER_NAME};
        }
    }
}